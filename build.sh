#!/bin/sh -e

TARGET=$JEHANNE/hacking/bin/
TRAMPOLINE=$JEHANNE/hacking/src/trampoline


(cd $TRAMPOLINE/lib/lib9/ && ./lib9.sh.build)
(cd $TRAMPOLINE/lib/bio/ && ./libbio.sh.build)
(cd $TRAMPOLINE/cmd/ && \
cc yacc.c -DPLAN9PORT -I$TRAMPOLINE/include -L$JEHANNE/hacking/lib -O0 -lbio -l9 -o $TARGET/9yacc)
(cd $TRAMPOLINE/cmd/ && \
cc dc.c -DPLAN9PORT -I$TRAMPOLINE/include -L$JEHANNE/hacking/lib -O0 -lbio -l9 -o $TARGET/dc)
(cp lib/yacc* $TARGET/../lib)
(cd $TRAMPOLINE/cmd/rc/ && ./rc.sh.build)
