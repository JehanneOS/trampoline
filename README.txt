A minimal plan9port fork designed just to cross-build Jehanne
=============================================================

The sole goal of these tools is to cross-build Jehanne in a way
that can be more easily ported to Jehanne itself. 


Tools available
---------------
- lib
  - lib9: stripped down lib9 from plan9port
          uses $JEHANNE/hacking instead of $PLAN9 to not mess with
          more useful plan9port installation
  - bio: plan9port libbio
  - yaccpar and yaccpars (parts of "yacc runtime")

- cmd
  - yacc (9yacc in $JEHANNE/hacking/bin)
  - rc

Commands will be installed in $JEHANNE/hacking/bin
Libraries will be installed in $JEHANNE/hacking/lib

WARNING
-------

Do not use these tools for anything else.
